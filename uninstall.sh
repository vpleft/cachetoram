#!/bin/bash

if [ $EUID -ne 0 ]; then
    echo "Please run this script as root!"
    exit 1
fi

systemctl disable --now cachetoram cachetoram_backup.timer
rm -f /etc/systemd/system/cachetoram.service \
        /etc/systemd/system/cachetoram_backup.service \
        /etc/systemd/system/cachetoram_backup.timer \
        /sbin/cachetoram
rm -rf /etc/cachetoram/

echo "Service stopped and uninstalled!"
exit 0