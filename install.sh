#!/bin/bash
set -e
if [ $EUID -ne 0 ]; then
    echo "Please run this script as root!"
    exit 1
fi

mkdir -p /etc/cachetoram/
install ./config /etc/cachetoram/config
install ./src/cachetoram /sbin/cachetoram

install ./src/cachetoram.service /etc/systemd/system/
install ./src/cachetoram_backup.service /etc/systemd/system/
install ./src/cachetoram_backup.timer /etc/systemd/system/
systemctl enable --now cachetoram cachetoram_backup.timer

echo "Service installed and started!"
echo "You can find the configuration in /etc/cachetoram/config"
exit 0